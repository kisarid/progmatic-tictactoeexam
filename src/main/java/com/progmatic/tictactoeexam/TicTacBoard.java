/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author progmatic
 */
public class TicTacBoard implements Board {

    private int boardSize;
    private PlayerType[][] cells;

    public TicTacBoard(int boardSize) {
        this.boardSize = boardSize;
        cells = new PlayerType[boardSize][boardSize];
        loadCells();
    }

    public TicTacBoard(int boardSize, PlayerType[][] cells) {
        this.boardSize = boardSize;
        this.cells = cells;
    }

    public PlayerType[][] getCells() {
        return cells;
    }

    public int getBoardSize() {
        return boardSize;
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        if (!isValidCoordinate(rowIdx, colIdx)) {
            throw new CellException(rowIdx, colIdx, "The given coordinates are out of bounds.");
        }

        return cells[rowIdx][colIdx];
    }

    @Override
    public void put(Cell cell) throws CellException {
        if (!isValidCoordinate(cell.getRow(), cell.getCol())) {
            throw new CellException(cell.getRow(), cell.getCol(), "The given cell does not fit on this board.");
        }

        PlayerType targetCell = cells[cell.getRow()][cell.getCol()];
        if (!targetCell.equals(PlayerType.EMPTY)) {
            throw new CellException(cell.getRow(), cell.getCol(), "The selected cell is not empty.");
        }

        cells[cell.getRow()][cell.getCol()] = cell.getCellsPlayer();
    }

    @Override
    public List<Cell> emptyCells() {
        List<Cell> emptyCells = new ArrayList<>();
        for (int row = 0; row < boardSize; row++) {
            for (int col = 0; col < boardSize; col++) {
                if (cells[row][col].equals(PlayerType.EMPTY)) {
                    emptyCells.add(new Cell(row, col, PlayerType.EMPTY));
                }
            }
        }
        return emptyCells;
    }

    @Override
    public boolean hasWon(PlayerType p) {
        return BoardUtils.checkCellsForWin(p, cells);
    }

    private void loadCells() {
        for (int row = 0; row < boardSize; row++) {
            for (int col = 0; col < boardSize; col++) {
                cells[row][col] = PlayerType.EMPTY;
            }
        }
    }

    private boolean isValidCoordinate(int row, int col) {
        return row >= 0 && row < boardSize && col >= 0 && col < boardSize;
    }

}
