/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.AbstractPlayer;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.List;

/**
 *
 * @author progmatic
 */
public class VictoryAwarePlayer extends AbstractPlayer {

    public VictoryAwarePlayer(PlayerType p) {
        super(p);
    }

    @Override
    public Cell nextMove(Board b) {
        TicTacBoard currentBoard = (TicTacBoard) b;
        List<Cell> emptyCells = currentBoard.emptyCells();
        if (emptyCells.isEmpty()) {
            return null;
        }

        PlayerType[][] copiedCells = BoardUtils.copyCells(b);
        for (Cell emptyCell : emptyCells) {
            int currentRow = emptyCell.getRow();
            int currentCol = emptyCell.getCol();
            copiedCells[currentRow][currentCol] = myType;
            
            if (BoardUtils.checkCellsForWin(myType, copiedCells)) {
                return new Cell(currentRow, currentCol, myType);
            } else {
                copiedCells[currentRow][currentCol] = PlayerType.EMPTY;
            }
        }

        return emptyCells.get(0);
    }

}
