/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;

/**
 *
 * @author progmatic
 */
public class PlayTicTacToe {

    public static void main(String[] args) throws CellException {

        TicTacBoard t = new TicTacBoard(3);

        t.put(new Cell(0, 2, PlayerType.O));
        t.put(new Cell(1, 1, PlayerType.O));
        t.put(new Cell(2, 0, PlayerType.O));
//        
        for (PlayerType[] cell : t.getCells()) {
            for (PlayerType playerType : cell) {
                System.out.print(playerType + " ");
            }
            System.out.println();
        }
        System.out.println();
        
        VictoryAwarePlayer v = new VictoryAwarePlayer(PlayerType.O);
        System.out.println(v.nextMove(t));
        System.out.println(BoardUtils.checkCellsForWin(PlayerType.O, t.getCells()));
        System.out.println(t.hasWon(PlayerType.O));

    }
}
