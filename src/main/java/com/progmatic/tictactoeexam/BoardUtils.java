/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;

/**
 *
 * @author progmatic
 */
public abstract class BoardUtils {
    
    public static PlayerType[][] copyCells(Board b) {
        TicTacBoard orig = (TicTacBoard) b;
        PlayerType[][] cells = new PlayerType[orig.getBoardSize()][orig.getBoardSize()];

        for (int row = 0; row < orig.getBoardSize(); row++) {
            for (int col = 0; col < orig.getBoardSize(); col++) {
                try {
                    cells[row][col] = orig.getCell(row, col);
                } catch (CellException ex) {
                    throw new RuntimeException("Coordinates error while copying cells." + ex);
                }
            }
        }
        return cells;
    }

    public static boolean checkCellsForWin(PlayerType p, PlayerType[][] cells) {
        return checkRows(p, cells) || checkCols(p, cells) || checkDiagonal(p, cells);
    }

    private static boolean checkRows(PlayerType p, PlayerType[][] cells) {
        for (int row = 0; row < cells.length; row++) {
            boolean check = true;
            for (int col = 0; col < cells.length; col++) {
                if (!cells[row][col].equals(p)) {
                    check = false;
                    break;
                }
            }
            if (check) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCols(PlayerType p, PlayerType[][] cells) {
        for (int col = 0; col < cells.length; col++) {
            boolean check = true;
            for (int row = 0; row < cells.length; row++) {
                if (!cells[row][col].equals(p)) {
                    check = false;
                    break;
                }
            }
            if (check) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkDiagonal(PlayerType p, PlayerType[][] cells) {
        boolean checkLeft = true, checkRight = true;

        for (int i = 0; i < cells.length; i++) {
            if (!cells[i][i].equals(p)) {
                checkLeft = false;
            }
            if (!cells[i][cells.length - 1 - i].equals(p)) {
                checkRight = false;
            }
        }
        return checkLeft || checkRight;
    }

}
